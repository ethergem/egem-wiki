---
title: Quarrynode Registration
description: 
published: true
date: 2022-11-10T19:33:20.967Z
tags: 
editor: markdown
dateCreated: 2019-12-01T20:42:25.509Z
---

# Requirements

- Tier 1 Node**: 10k (ten thousand) or more EGEM on a registered wallet

## Example
- An **Ubuntu 18.04 VPS** with **IPv4** for each tier
- Tier 1 (10000 EGEM, 1 VPS, 1 IPv4)

# Register
> The system allows you to use discord to manage your nodes and system features.
{.is-success}

## Discord
https://discord.egem.io

Join the discord server and look for our bot called "The EGEM Master" then send it a private message to start using system.

![egembot.png](/egembot.png)

# Add a node
[Full Bot Command List](https://wiki.egem.io/botcmds)  
From within discord use the following command.
> WARNING!!! It is very important to make sure tier 1 is selected for tier, and the node number is from 1 to 20.
{.is-warning}

```/addnode tier number ipaddress```
## Examples
Here are some examples of certain scenarios for users and nodes with required balances.
> **It is recommended to make a list of your nodes and IP before you add them to the bot**
{.is-warning}


> **A user with 10,000+ EGEM**
> /addnode tier number ipaddress
> /addnode 1 1 192.168.1.1
{.is-success}


> **A user with 50,000+ EGEM**
> /addnode tier number ipaddress
> /addnode 1 1 192.168.1.1
> /addnode 1 2 192.168.1.5
> /addnode 1 3 192.168.1.7
> /addnode 1 4 192.168.1.9
> /addnode 1 5 192.168.1.10
{.is-success}

# Remove a node
It is very simple to remove a node from your account.

```/remnode tier number```


# Add Address
Assign the EGEM address so system can watch balance and send transactions to it.

```/setaddress egemaddress```

# Set Paylimit
Pick from 1-100 for when the system will send the transaction containing the EGEM earned when it reaches the limit.

```/paylimit 1-100```

# Toggle Autopay
Set if you want the system to send automatic transactions for earned EGEM.

```/autopay```