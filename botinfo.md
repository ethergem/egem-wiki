---
title: Discord bot Information
description: 
published: true
date: 2022-11-03T19:10:27.785Z
tags: 
editor: markdown
dateCreated: 2019-12-04T15:14:09.805Z
---

# The Discord bot
![egembot.png](/egembot.png)

The Discord bot was written from the ground up by Riddlez666
with node.js and the discord.js library. The bot uses mysql for the
database.

# Games
The bot has games as well where you can win Egem.

## Trivia

There is a trivia game in bot-trivia.
This game is in the bot-trivia channel.

![950.png](/950.png)
## Dice roll
This game is temporarily disabled.

![roll.png](/roll.png)

## Slot machine
This game is in the bot-games channel.

![slots.png](/slots.png)

---
The bot can also be a little sassy at times.

![sassybot.png](/sassybot.png)

## Commands

Information on the Discord commands can be found at
https://wiki.egem.io/botcmds