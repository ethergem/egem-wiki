---
title: Send Transactions
description: 
published: true
date: 2020-03-21T18:29:57.618Z
tags: 
---

# Send Transactions
How to send Ethergem (EGEM).

## MyEgemWallet
This domain is no longer part of the network


### Ledger
- Add Ethergem to Ledger and Select EGEM from Hardware Wallet
- Start Ledger Live App
- Connect to Ledger Wallet
- Select Address to Send From and Unlock Wallet
- Fill out To Address and Amount to Send and Click Generate Transaction
- Click Checkmark on Ledger Hardware Wallet
- Click Send Transaction
- Click Yes, I am sure! Make Transaction.
- Success!

### Trezor
### Metamask

## MyEtherWallet
[myetherwallet](https://wiki.egem.io/egemwallet#mew)

### Ledger
### Trezor
### Metamask

## Metamask
[metamask](https://wiki.egem.io/egemwallet#metamask)

## Opal