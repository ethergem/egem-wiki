---
title: Technical Information
description: 
published: true
date: 2024-01-30T15:53:56.612Z
tags: 
editor: markdown
dateCreated: 2019-12-01T21:01:12.637Z
---

# About Quarrynodes

- Custom masternode system using [discord.js](https://discord.js.org/#/) / [web3.js](https://web3js.readthedocs.io/en/1.0/) / [telegraf.js](https://telegraf.js.org/) with a MySQL database for storage.

https://gitlab.com/ethergem/egem-bot

## Features
Just some of the minor things to mention.
- Privacy due to the private message nature of the system.
- Reliable network of updatable nodes to maintain stability and security.
- Almost all parts of the system have been designed by members of the community and their input.


# Collateral Requirements
## Quarrynode
> 10,000 EGEM Collateral
{.is-success}





# Hardware Requirements

Minimum Requirements:

> CPU: 2 core
> RAM: 4 GB total between RAM + swap
> Disk: 40 GB HDD/SDD
> Bandwidth: 250 GB per month
> Operating System: Ubuntu 20/22, CentOS 7, Debian 9 or Raspbian 9
{.is-success}

