---
title: Mining Information
description: 
published: true
date: 2022-11-11T19:37:11.689Z
tags: 
editor: markdown
dateCreated: 2019-12-01T20:27:55.343Z
---

# ASIC Miners
TODO

# GPU Miners
BMiner
https://www.bminer.me/

Claymore Dual Miner
https://bitcointalk.org/index.php?topic=1433925.0

ETH Miner
https://github.com/ethereum-mining/ethminer/releases

PhoenixMiner
https://bitcointalk.org/index.php?topic=2647654.0

# CPU MINERS
N/A

# OS Miners
ethOS Mining OS
http://ethosdistro.com/

Hive OS
https://hiveos.farm/

SMOS
https://simplemining.net/

# Windows 10
Overview

**Privacy Settings**

Make sure to uncheck all the options to reduce the number of services running in the background.

![privacy-settings.jpeg](/win10guide/privacy-settings.jpeg)
Privacy Settings

After the installation is done, go to Settings then download all the Windows updates, afterward install your GPU Drivers, 
Make sure you get the right model for your cards when you install the drivers, and always install the latest official download(not beta), 

Finish installing all the updates, then install these applications:

GPU-Z – For sensors on your GPU such as temperatures.
Now reboot your Mining Rig and make sure ALL the updates are installed.

Configuring Your Settings

Let's start with the Control Panel:

Navigate to Control Panel > System and Security > System and change these 2 settings:

**Control Panel**
![control-panel.png](/win10guide/control-panel.png)
Control Panel

Navigate to the Advanced tab, click on Performance, and change to Performance mode.

Advanced Performance
![advanced-performance.png](/win10guide/advanced-performance.png)
Advanced Performance

Go to Advanced in the same box that popped up after the first step and then go to Memory Latency – Change it to 2000MB Per GPU you have for Initial, and for the Max 3000MB Per GPU.

Virtual Memory
![virtual-memory.png](/win10guide/virtual-memory.png)
Virtual Memory
Example: You have 3 RX 580s, which would be 6000MB for Initial, 9000MB Max.

**Power Settings**

Now we move on to the power settings.

Open Power Settings > Advanced Power Settings
Select High-Performance settings
Change plan settings
Navigate to Advanced Power Settings
Set PCI Express Link power management to Off (refer to the picture below)

Power Settings
![power-settings.jpg](/win10guide/power-settings.jpg)
Power Settings

Registry Tweaks (advanced users only)

The last step in the setup is to download the file that will be linked below. The file optimizes your registry, you will type ‘y‘ for usually all of it, but if you do not want a certain thing disabled feel free to type ‘n‘. The purpose is to stop a lot of the background services that would impact your mining stability & removes all the extra apps that are not needed. It does turn off windows updates but whenever you want to turn it back on to update all you have to do is head to the registry (mscv) and resume the Windows Update service. All the credits go to the creator of the script, his tag is mentioned in it. Make sure to run it as an Admin. 

[windows_10_registry_tweaks_for_mining.txt](/win10guide/windows_10_registry_tweaks_for_mining.txt)
Rename file to .bat


Great! Now you are ready to start mining. First want to make sure to overclock/undervolt your card properly for whichever mining algorithm you are going for and then you can choose your Mining Software and get started with it.

For overclocking, here is a full video guide on the topic to explain if it damages your hardware or not and what are the benefits of doing it.

Mining Software
Cryptocurrency Mining software brings fear to the users due to false flags that all Anti-viruses trigger when you download or try to use any of the mining software. The reason being is that most of the mining software has been used maliciously, therefore, it resulted in all of them being blacklisted. Which means they are automatically detected as viruses and exterminated.

As a user, you have every right to be worried but there are steps that you can take to be more confident with using different mining software, then after that, I will show you how to avoid having your mining software automatically get deleted all the time by your Anti-Virus.

Now before we get to the instructions, you first need to understand the different types of software.

Types Of Mining Software
There are generally two types of mining software. One for direct mining with lower fees and the other makes direct mining easier but takes more fees.

Command-line Interface
First, there is Command-line Interface [CLI] software such as Phoneixminer, NBminer, T-Rex Miner, etc. These are run in a command prompt so they don’t have a user interface.

NBMiner
![nbminer_mining_screen.png](/win10guide/nbminer_mining_screen.png)
NBMiner
Some have anonymous developers and some don’t. Phoenixminer for example is run by anonymous developers and for most people that worry them even more. Now how I like to look at that is first to make sure the mining software has a clean history. Meaning that the anonymous team behind the software did not try to intentionally damage their users before.

Now keep in mind that there are scenarios where impostors will copy their software and tamper with it, and that is why you always need to verify with the Checksums which will be covered after.

Back to the Phoenixminer example. The mining software has been running for years and has not yet damaged its users in any way and then they were caught. One thing that I am sure will keep it that way is that the incentives they get from you using their software are much more rewarding than them ruining their reputation for a one-time benefit.

Graphical-Interface Software
Also known as GUI Miners,  these applications bundle different CLI Miners and then provide you with an interface for ease of use but in return, they take a higher percentage of fees such as Kryptex, Cudominer, Minerstat, etc.

Kryptex Miner
![kryptex-miner.jpg](/win10guide/kryptex-miner.jpg)
Kryptex Miner
These simply just give you an interface for using the different CLI Miners and most of the time they do not require exclusions in the anti-virus because they automatically set them upon installation, and they also do not require verifying the checksums which we will go over in a second

Verifying Checksums (CLI Only)
When you download the zip folder of CLI mining software, it is wise to verify the checksums to make sure you have an authentic version of the software. Most of the time there will be a checksums file that has different strings that are used for verification, some CLI software doesn’t have checksums in that case you can skip this step. These checksums are related to a certain file or folder.

TeamRedMiner Checksums
TeamRedMiner Checksums
In the example above, TeamRedMiner checksums are for the teamredminer.exe file. So that means you will have to extract the ZIP Folder so you can have access to that .exe file.

1. Open the command prompt [not as admin] and navigate to the directory that you installed the Miner Zip folder in using the cd command.

Command Prompt
![cmd.jpg](/win10guide/cmd.jpg)
Command Prompt
2. Find out what file or folder is the checksums for and then navigate to that with the cd command. So since TeamRedMiner.exe is inside the folder, we first will extract it then we will use the cd command to get into the folder.

If your antivirus is blocking you from extracting, then move on to the Antivirus exclusion step first.

Tip: use the tab key to autofill the name. i.e. I type: teamre then I hit tab and it will autofill the name to the closest thing in that folder the command prompt is open in.

Locating_Directory
![locating_directory.jpg](/win10guide/locating_directory.jpg)
Locating_Directory
3. Run this command: certUtil -hashfile <file/folder name> <algorithm>

CertUtil_Command
  ![certutil_command.jpg](/win10guide/certutil_command.jpg)
CertUtil Command
4. Compare the string that you receive from that command with the checksums provided with the mining software. Usually, it is on the GitHub page or in a checksums.txt file.

Verifying Checksums
  ![verifying-checksum.jpg](/win10guide/verifying-checksum.jpg)
Verifying Checksums
Antivirus Exclusions
To add exclusions, all you need to do is make a folder where ever you want, in this example, I use my Desktop folder and I make a new folder in there named Miners.


After that, go to your antivirus, whichever your using; in this case, it is Windows Defender and add the folder to the excluded list.

Windows Exclusions
  ![windows-defender-exclusions.jpg](/win10guide/windows-defender-exclusions.jpg)
Windows Exclusions
Now you can save your mining software directly in that folder without any trouble. If your browser blocks your download, then click on the bottom right and allow it from the browser downloads section.

***Please make sure you're getting the mining software from authentic sources. It is never 100% safe so always be vigilant.***

# Startup Lines

## - Ethminer
### Linux
./ethminer -P stratums://0x87045b7badac9c2da19f5b0ee2bcea943a786644.RIGNAME@pool.egem.io:PORT 

### NVIDIA
ethminer.exe -S pool.egem.io:8004 -O 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7.rigname -U -SP 1

### AMD/ATI
ethminer.exe -S pool.egem.io:8004 -O 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7.rigname -G -SP 1

## - CLAYMORE

### NVIDIA
EthDcrMiner64.exe -epool stratum+tcp://pool.egem.io:8008 -ewal 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7 -epsw x -allpools 1 -allcoins 1 -gser 2 -eworker rigname

SSL/TLS: (example: -epool ssl://pool.egem.io:PORT) 

## - PHOENIX MINER

### NVIDIA and AMD
PhoenixMiner.exe -pool pool.egem.io:8008 -wal 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7 -worker rigname

SSL/TLS: (example: -pool ssl://pool.egem.io:PORT) 