---
title: Technical Information
description: 
published: true
date: 2022-11-03T18:40:54.177Z
tags: 
editor: markdown
dateCreated: 2019-12-01T20:22:41.582Z
---

# EtherGem / EGEM

The heart and brains of our network for sapphire to leverage, but thats only the start of the network ties.

Algorithm: PoW Dagger-Hashimoto 
# Blocktime
Target: 11-13 seconds
# Ticker Info
Name: EtherGem
Ticker: EGEM 
# Chain Id
Network Id: 1987
Chain Id: 1987
# Ports
RPC:  8895
P2P: 30666
# External Links
BIP-0044
https://github.com/satoshilabs/slips/blob/master/slip-0044.md 

Official Ann
https://bctann.egem.io
https://bitcointalk.org/index.php?topic=3167940.0

# Block rewards
Block rewards will remain the following for the foreseeable future:
Node: 1.5, Miners: 3, Devs: 0.5
# Supply
Currently there are around 49 million with roughly 7 million per year added at the current block reward
# Developer Pay Breakdown
TODO