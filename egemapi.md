---
title: EtherGem API
description: 
published: true
date: 2021-06-09T15:18:17.269Z
tags: 
editor: markdown
dateCreated: 2019-11-29T18:43:42.007Z
---

- This is the API used to get information on the EGEM network and the quarrynodes.

# General Usage

## Get Balance
`https://api.egem.io/account?addr=EXAMPLEADDRESS`
## Get Transaction
`https://api.egem.io/tx?hash=EXAMPLEXHASH`
## Get Block Number
`https://api.egem.io/block?number=EXAMPLEBLOCKNUMBER`
## Get Block Hash
`https://api.egem.io/block?hash=EXAMPLEHASH`
## Get Uncle Hash
`https://api.egem.io/uncle?number=&index=EXAMPLEINDEXNUMBER`
## Get Uncle Number
`https://api.egem.io/uncle?hash=&index=EXAMPLEHASH`
## Get Contract Code
`https://api.egem.io/code?addr=EXAMPLECONTRACTADDRESS`
## Get Signed Transaction
`https://api.egem.io/sendsigned?hex=EXAMPLETXHASH`
## Get Estimated Supply
`https://api.egem.io/estsupply`
## Get Explorer Stats
`https://explorer.egem.io/api/stats`

# Bot Requests

## Bot Users
`https://api.egem.io/botusers`
## Earning Users
`https://api.egem.io/nodelist`

