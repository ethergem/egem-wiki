---
title: How to help.
description: 
published: true
date: 2019-12-16T18:44:56.892Z
tags: 
---

# How to help
## Time
One good way to help spread reach is to comment daily on our BCT ANN as it draws more new eyes, and sharing on twitter.

## Finacial Support
We are not above or below accepting donations from users looking to see EGEM succede, but we would rather your time or code to help first and foremost.

## Code
Anyone willing to contribute code that has a stake of egem will no doubt benefit from helping the community grow and expand.