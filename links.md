---
title: Useful Links
description: Links for EGEM- Gathered by BuzzkillB
published: true
date: 2021-07-25T21:23:46.296Z
tags: 
editor: markdown
dateCreated: 2019-12-01T18:51:28.146Z
---

## Official Ethergem [EGEM]
[Website](https://egem.io/)  
[Gitlab](https://git.egem.io/team)  
[Bitcointalk ANN](https://bitcointalk.org/index.php?topic=3167940.0)  
[EGEM Meta](https://git.egem.io/team/egem-meta)  
[Network Stats](http://network.egem.io/)  
[Quarry Node Stats](https://egem.io/nodelist)  
[Main Explorer](https://blockscout.egem.io)
[Backup Explorer](https://explorer.egem.io/)   
[Whattomine](https://whattomine.com/coins/307-egem-ethash)
[YouTube](https://www.youtube.com/c/egemofficial)

## Official Sapphire [SFRX]  

[Bitcointalk ANN](https://bitcointalk.org/index.php?topic=5063954.0)  
[5 Sec BTC Proof of Concept](https://github.com/osoese/5SECPDF/blob/master/5-second-btc-proof-of-concept%20.pdf)  

## Social Media  
[Reddit](https://www.reddit.com/r/egem/)  
[Discord](https://discord.gg/39WGWRB)  
[Facebook](https://www.facebook.com/Ethergem)  
[Twitter](https://twitter.com/ETHERGEMCOIN)  
[Telegram](t.me/egemofficial)  

## Wallets

[Opal Wallet](https://git.egem.io/team/Opal-Alpha-0-0-1)  
[https://www.myetherwallet.com/](https://www.myetherwallet.com/)  
Set custom network on Metamask -> https://lb.rpc.egem.io  
[Ledger](https://support.ledger.com/hc/en-us/articles/360009611074-EtherGem-EGEM-)  
[Trezor](https://trezor.io/coins)  
[Docker](https://hub.docker.com/r/ethergem/go-egem)  

## Exchanges
[Graviex](https://graviex.net/signup?ref=7514d15b2a5d51375eb7b135)
[Stex](https://app.stex.com/en/trade/pair/BTC/EGEM)
[Mercatox](https://mercatox.com/?referrer=667754)



**Stex** [https://app.stex.com/en/trade/pair/BTC/EGEM/1D](https://app.stex.com/en/trade/pair/BTC/EGEM/1D)  

## Market Info
[https://www.coingecko.com/en/coins/ethergem](https://www.coingecko.com/en/coins/ethergem)  
[https://www.cheddur.com/coins/ethergem](https://www.cheddur.com/coins/ethergem)  
[https://coinmarketcap.com/currencies/ethergem/](https://coinmarketcap.com/currencies/ethergem/)  
[https://coinmarketcap.com/converter/egem/usd/](https://coinmarketcap.com/converter/egem/usd/)  
[https://www.livecoinwatch.com/price/EtherGem-EGEM](https://www.livecoinwatch.com/price/EtherGem-EGEM)  
[https://walletinvestor.com/currency/ethergem](https://walletinvestor.com/currency/ethergem)  
[https://www.worldcoinindex.com/coin/ethergem](https://www.worldcoinindex.com/coin/ethergem)  
[http://cryptofresh.com/a/EGEM](http://cryptofresh.com/a/EGEM)  
[https://coincodex.com/crypto/ethergem/](https://coincodex.com/crypto/ethergem/)  
[https://coinlib.io/coin/EGEM/EtherGem](https://coinlib.io/coin/EGEM/EtherGem)  

## Mining Pools  
[https://mining.alekseirubin.com/calculator/EGEM?hashrate=100&diffPage=current](https://mining.alekseirubin.com/calculator/EGEM?hashrate=100&diffPage=current)  
[https://git.egem.io/team/egem-pools/blob/master/masterlist.json](https://git.egem.io/team/egem-pools/blob/master/masterlist.json)  
[Pool Explorer](https://poolexplorer.com/coin/4970)  
[https://miningpoolstats.stream/ethergem](https://miningpoolstats.stream/ethergem)  
[http://pool.egem.io/](http://pool.egem.io/)  

## Research  
[Top Cryptocurrency Projects Part 2: ETHERGEM CALLISTO DASH](https://www.publish0x.com/cryptokeeper/top-cryptocurrency-projects-part-2-ethergem-callisto-dash-xmmwm)  

## Articles
[SFRX + XBI partnership](https://medium.com/@btcixbi/first-major-xbi-partnership-the-road-to-adoption-fa6ae4ccc228)
[EtherGem July 2019 Update](https://medium.com/@egem.cm/egem-july-update-d5cb71d14957)  
[EtherGem May 2019 Update](https://medium.com/@egem.cm/ethergem-may-update-c971237605d9)  
[EtherGem February 2019 Update](https://medium.com/@egem.cm/february-update-7ac9771e8531)  
[EtherGem September 2018 Update](https://medium.com/@egem.cm/egem-september-update-4aae213a862e)  

## Video/Audio Streams
(2021-05-09) State Of The Project - EtherGem - #2 May 2021 featuring dev 'Osoese ' [YouTube](https://www.youtube.com/watch?v=8yFeyWAP590)
(2021-04-16) State Of The Project - EtherGem - #1 April 2021 featuring dev 'Osoese' [YouTube](https://www.youtube.com/watch?v=JjWqofZp4lk)
(2021-03-21) Connect Egem to MetaMask Tutorial [YouTube](https://www.youtube.com/watch?v=ySGt7NdSHhM)
(2021-02-17) Egem Project Overview [YouTube](https://www.youtube.com/watch?v=I6YfAJchufc)
(2020-02-20) Latest interview on EGEM/Sapphire with Frank and Troy by mineyourbiz - [Part 1](https://anchor.fm/mineyourbiz/episodes/Frank--Troy-from-EtherGem-1-of-2-eaupld)
(2020-02-21) Latest interview on EGEM/Sapphire with Frank and Troy by mineyourbiz - [Part 2](https://anchor.fm/mineyourbiz/episodes/Frank--Troy-from-EtherGem-2-of-2-eavjng)
(2019-05-31) How to Mine EtherGem (EGEM) LIVE! Â  [YouTube](https://www.youtube.com/watch?v=ktTdfPMMPD8)  
(2019-05-29) SFRX Lets do a transaction and record  [YouTube](https://www.youtube.com/watch?v=CMD6ytnCpdc)  
(2019-05-27) SFRX Just quick update  [YouTube](https://www.youtube.com/watch?v=bc056JJmycc)  
(2019-05-27) SFRX Random drag wallet creation  [YouTube](https://www.youtube.com/watch?v=r9ZPDLt4HY8)  
(2019-05-26) SFRX Acct modal & some progress  [YouTube](https://www.youtube.com/watch?v=tNAwVSAJWWs)  
(2019-05-24) SFRX latest  [YouTube](https://www.youtube.com/watch?v=9ibflZwromY)  
(2019-05-15) SFRX My test of sapphire using mobile app  [YouTube](https://www.youtube.com/watch?v=KjxYbnaYqj4)  
(2019-05-11) SFRX mobile testing 051119  [YouTube](https://www.youtube.com/watch?v=dBJp9a_WNMY)  
(2019-05-09) SFRX mobile testing 050919  [YouTube](https://www.youtube.com/watch?v=Z5EAgbhOc44)  
(2019-03-22) ITK Crypto #10 - Ethergem- featuring Osoese & Tbates76  [YouTube](https://youtu.be/lymppbvjOes)  
(2019-03-20) EtherGem (EGEM)Â¸ Nvidia (algo Ethash) [YouTube](https://youtu.be/ifB2XEHF8TM)  
(2018-11-19) Bitcoin Incognito & EGEM Monsters feat. Galimba, Sys, NotFreight  [Spotify](https://open.spotify.com/episode/6BfBVLwb5KmsboCG7HVt4Y?si=zYGDR_cwQims-eQbbI4TRg)  
(2018-11-06) Masari & EGEM SFRX feat. Thaer  [Spotify](https://open.spotify.com/episode/3ybwALVvJnMmHtp27l3cG2)  
(2018-10-22) Coach Cryptos Proof of Work Podcast  [Bittube](https://bit.tube/play?hash=QmS1SUmuXFPNhREocoRL5o324NtpzxrNDSfM2HGwK5b7JS&channel=20427)  [Google Podcast](https://www.google.com/podcasts?feed=aHR0cHM6Ly9hbmNob3IuZm0vcy80MzRiYmMwL3BvZGNhc3QvcnNz)  [Radiopublic](https://radiopublic.com/proofofwork-podcast-8gvOQK)  [Breaker](https://www.breaker.audio/proof-of-work-podcast)  [Pcast](https://pca.st/u7Qm)  

